const path = require('path');
const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;
const data = require('./public/data.json');


app.use(express.static('public'));

// return simple json object
app.get('/data', (req, res) => {
	res.json(data);
});

// if route does not exist, error will be sent
app.all('*', function(req, res) {
    res.status(404).send("404 Bad request");
});

app.listen(PORT, function(){
	console.log("server is running on port: " + PORT)
})