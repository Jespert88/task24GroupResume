## Porfolio skeleton
    Template was used from https://www.codibly.com/
    It was redesign.


## Heroku
    https://jesper-cv.herokuapp.com/

# Change values in this file to make changes

    If you do not have organisation, leave it empty (delete key's though)
    {
        "firstName": "",
        "lastName": "",
        "age": "",
        "desc": "",
        "nationality": "",
        "image": "",
        "contactInformation": {
            "email": "",
            "phoneNumber": ""
        },
        "location": {
            "country": "",
            "city": ""
        },
        "languages": [
        {
            "language": "",
            "competenceLevel": ""
        }
        ],
        "links": [
        ],
        "workExperience": [
        {
            "title": "",
            "company": "",
            "startDate": "",
            "endDate": "",
            "desc": ""
        }
        ],
        "education": [
        {
            "title": "",
            "school": "",
            "startDate": "",
            "endDate": "",
            "desc": ""
        }
        ],
        "organisations": [
        {
            "title": "",
            "organisation": "",
            "startDate": "",
            "endDate": "",
            "desc": ""
        }
        ],
        "skills": [
        ],
        "interests": [
        ]
    }