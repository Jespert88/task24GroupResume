// load navigation bar
$(document).ready(function(){
	$('#nav-content').load("components/navbar.html");
});	

// load home / about content for the first time
$(document).ready(function(){
	getAbout()
});	

// collapses "burger" list in navigation bar after click
function hideNavBar(){
    $('.navbar-collapse').collapse('hide');
}

// load about content
function getAbout(){
    hideNavBar()
    
    $('#main-content').load("components/aboutContent.html", function() {
        // load home / about data here
        $.getJSON('data.json', function(data) {
            $("#facePicture").attr("style", "background-image:url(" + data.image + ")")
            $("#fullName").html(data.firstName + " " + data.lastName)
            $("#location").html(data.location.country + " " + data.location.city)
            $("#age").html(data.age)
            $("#email").html(data.contactInformation.email)
            $("#email").attr("href", "mailto:" + data.links.linkedin)

            $("#phoneNumber").html(data.contactInformation.phoneNumber)

            let linksData = ""
            data.links.forEach(function(obj) {
                linksData += "<a href=" + obj +" target='_blank'>" + obj + "</a><br><br>"
            })
            $("#links").html(linksData)

            $("#about-text").html(data.desc)
            $("#contentFullName").html(data.firstName + " " + data.lastName)   

            let skillsData = ""
            data.skills.forEach(function(obj) {
                skillsData += "<li><i class='fas fa-asterisk'></i> " + obj + "</li>"
            })
            $(".skillsList").html(skillsData)

            let languagesData = ""
            data.languages.forEach(function(obj) {
                languagesData += "<li><i class='fas fa-asterisk'></i> " + obj.language + ": " + obj.competenceLevel + "</li>"
            })
            $(".languagesList").html(languagesData)

            let interestData = ""
            data.interests.forEach(function(obj) {
                interestData += "<li><i class='fas fa-asterisk'></i> " + obj + "</li>"
            })
            $(".interestList").html(interestData)


        })
    });
}

// load education content
function getEducation(){
    hideNavBar()
    $('#main-content').load("components/educationContent.html", function() {
    	// load education data here
        $.getJSON('/data.json', function(data) {
            let educationData = ""
            data.education.forEach(function(obj) {
                educationData +="<div class='edu_item d-flex flex-lg-row flex-column align-items-start'>" +
                                    "<div><div class='edu_year'>" + obj.startDate+ "-" + obj.endDate + "</div></div>" +
                                    "<div class='edu_content'>" +
                                        "<div class='edu_title_container'>"+
                                            "<div class='edu_title'>" + obj.title + "</div>" +
                                            "<div class='edu_subtitle'>" +obj.school + "</div>" +
                                        "</div>" +
                                        "<div class='edu_text'>" +
                                            "<p>" + obj.desc + "</p>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>"
            })
            educationData += "<br><br>"
            $(".education").html(educationData)
        })
    });
}

// load work experience content
function getWorkExperience(){
    hideNavBar()
    $('#main-content').load("components/experienceContent.html", function() {
    	// load experience data here
        $.getJSON('/data.json', function(data) {
            let experienceData = ""
            data.workExperience.forEach(function(obj) {
                experienceData +="<div class='exp_item d-flex flex-md-row flex-column align-items-start'>" +
                                    "<div><div class='exp_time'>" + obj.startDate+ "-" + obj.endDate + "</div></div>" +
                                    "<div class='exp_content'>" +
                                        "<div class='exp_title_container'>"+
                                            "<div class='exp_title'>" + obj.title + "</div>" +
                                            "<div class='exp_subtitle'>" +obj.company + "</div>" +
                                        "</div>" +
                                        "<div class='exp_text'>" +
                                            "<p>" + obj.desc + "</p>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>"
            })
            $(".experience").html(experienceData)


            // If one does not have organisations it will not be shown.
            if(data.organisations.length != 0) {
                let organisationData = ""
                organisationData += "<div class='main_title_container d-flex flex-column align-items-start justify-content-end'>" +
                                        "<div class='main_title'>Organisations</div>" +
                                    "</div>" +
                                    "<div class='main_content_scroll mCustomScrollbar' data-msc-theme='minimal-dark'>" 
                data.organisations.forEach(function(obj) {
                    organisationData +="<div class='exp_item d-flex flex-md-row flex-column align-items-start'>" +
                                        "<div><div class='exp_time'>" + obj.startDate+ "-" + obj.endDate + "</div></div>" +
                                        "<div class='exp_content'>" +
                                            "<div class='exp_title_container'>"+
                                                "<div class='exp_title'>" + obj.title + "</div>" +
                                                "<div class='exp_subtitle'>" +obj.organisation + "</div>" +
                                            "</div>" +
                                            "<div class='exp_text'>" +
                                                "<p>" + obj.desc + "</p>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div></div>"
                })
                organisationData += "<br><br>"
                $(".organisations").html(organisationData)
            }
        })
    });
}

// load contact content
function getContact(){
    hideNavBar()
    $('#main-content').load("components/contactContent.html", function() {
    	// load contact data here
    });
}